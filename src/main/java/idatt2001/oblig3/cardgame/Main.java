package idatt2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * JavaFX class containing GUI for Poker application. A player can draw five
 * random cards from a deck of cards and add them to the hand. The hand can be "checked",
 * showcasing four attributes: The sum of the faces, Flush(true/false), Queen of spades
 * in hand(true/false), show all cards of hearts.
 *
 * @author Sindre Glomnes
 * @version 2021-04-05
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        HandOfCards hand = new HandOfCards();

        stage.setTitle("PokerApp");
        stage.getIcons().add(new Image("file:src/main/resources/spades.jpg"));

        //Display card text
        Text cards = new Text();
        cards.setX(40);
        cards.setY(75);
        cards.setFont(Font.font("Verdana",25));

        //Display: "Sum of the faces:"
        Text sum = new Text();
        sum.setText("Sum of the faces: ");
        sum.setX(40);
        sum.setY(200);
        sum.setFont(Font.font("Verdana",12));

        //Display: "Cards of hearts: "
        Text hearts = new Text();
        hearts.setText("Cards of hearts: ");
        hearts.setX(40);
        hearts.setY(240);
        hearts.setFont(Font.font("Verdana",12));

        //Display: "Flush: "
        Text flush = new Text();
        flush.setText("Flush: ");
        flush.setX(250);
        flush.setY(200);
        flush.setFont(Font.font("Verdana",12));

        //Display: "Queen of spades: "
        Text queen = new Text();
        queen.setText("Queen of spades: ");
        queen.setX(250);
        queen.setY(240);
        queen.setFont(Font.font("Verdana",12));

        //Draw card button
        Button drawCard = new Button();
        drawCard.setText("Draw cards");
        drawCard.setLayoutX(300);
        drawCard.setLayoutY(40);
        drawCard.setOnAction(actionEvent -> {
            hand.drawCards(5);
            cards.setText(hand.showCards());
            sum.setText("Sum of the faces: ");
            hearts.setText("Cards of hearts: ");
            flush.setText("Flush: ");
            queen.setText("Queen of spades: ");
        });

        //Check hand button
        Button checkHand = new Button();
        checkHand.setText("Check hand");
        checkHand.setLayoutX(300);
        checkHand.setLayoutY(80);
        checkHand.setOnAction(actionEvent -> {
            sum.setText("Sum of the faces: " + hand.getSum());
            hearts.setText("Cards of hearts: " + hand.getHearths());
            flush.setText("Flush: " + hand.checkFlush());
            queen.setText("Queen of spades: " + hand.isQueenOfSpade());

        });

        Group root = new Group();
        root.getChildren().add(drawCard);
        root.getChildren().add(checkHand);
        root.getChildren().add(cards);

        root.getChildren().add(sum);
        root.getChildren().add(hearts);
        root.getChildren().add(flush);
        root.getChildren().add(queen);

        stage.setScene(new Scene(root, 400, 300, Color.LIGHTSKYBLUE));
        stage.show();
    }
}
