package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of cards. A deck of cards contains a total of 52 cards. 13 cards
 * of each type; spades, hearts, diamonds, and clubs. All cards have their own
 * combination of suit and face.
 *
 * @author Sindre Glomnes
 * @version 2021-04-05
 */
public class DeckOfCards {
    ArrayList<PlayingCard> cards = new ArrayList<>(); // All 52 PlayingCards
    private final char[] suit = {'♠','♥','♦','♣'};
    private final int[] face = {1,2,3,4,5,6,7,8,9,10,11,12,13};

    /**
     * Creates a total 52 PlayingCard's and adds them to the ArrayList cards.
     * Every combination of suit's and face's adds up to a total of 52.
     */
    public DeckOfCards() {
        for (char c : suit) {
            for (int i : face) {
                cards.add(new PlayingCard(c, i));
            }
        }
    }

    /**
     * Returns a ArrayList of n numbers of random PlayingCard's.
     * The number of cards returned must be between 5 and 52 to be valid.
     * Duplicates of the same PlayingCard is not possible.
     *
     * @param n int number between 5 and 52.
     * @return ArrayList of n random cards.
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if (n>52) {
            n = 52;
        }
        if (n<5) {
            n = 5;
        }

        ArrayList<PlayingCard> randomCards = new ArrayList<>();

        while (randomCards.size() < n) {
            Random r = new Random();
            int randomCard = r.nextInt(52);
            if (!(randomCards.contains(cards.get(randomCard)))) {
                randomCards.add(cards.get(randomCard));
            }
        }
        return randomCards;
    }

    /**
     * Returns the ArrayList cards (the whole deck of cards).
     *
     * @return All 52 PlayingCard's
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }
}
