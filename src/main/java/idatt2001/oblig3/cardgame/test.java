package idatt2001.oblig3.cardgame;

public class test {
    public static void main(String[] args) {

        HandOfCards hand = new HandOfCards();

        hand.drawCards(10);

        System.out.println("Your cards: " + hand.showCards());

        System.out.println("Is flush: " + hand.checkFlush());

        System.out.println("Sum of cards: " + hand.getSum());

        System.out.println("All the hearths: " + hand.getHearths());

        System.out.println("Queen of spade in deck: " + hand.isQueenOfSpade());

    }
}
