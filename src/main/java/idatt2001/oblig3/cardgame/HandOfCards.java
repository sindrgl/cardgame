package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a hand of cards. A player can draw x amount of cards
 * and check it's attributes: The sum of faces, flush (five of the same suit),
 * show all hearts, and check if the Queen of spades is drawn.
 *
 * @author Sindre Glomnes
 * @version 2021-04-05
 */
public class HandOfCards {

    List<PlayingCard> hand = Collections.emptyList(); // All PlayingCards in hand
    DeckOfCards deck = new DeckOfCards();

    /**
     * Creates an instance of a HandOfCards, containing the cards
     * of the users choice.
     *
     * @param hand ArrayList of PlayingCards
     */
    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Creates an instance of a HandOfCards.
     */
    public HandOfCards() {}


    /**
     * Draws n numbers of random PlayingCard's from a DeckOfCards, and adds them
     * to the hand.
     *
     * @param n number of random cards to be added.
     */
    public void drawCards(int n) {
        hand = deck.dealHand(n);
    }

    /**
     * Returns a String of all PlayingCard's in the hand.
     *
     * @return PlayingCards in hand as String.
     */
    public String showCards() {
        String cardsString = "";
        for (PlayingCard p: hand) {
            cardsString += p.getAsString() + " ";
        }
        return cardsString;
    }

    /**
     * Checks if the hand has a flush, meaning it contains
     * five cards of the same suit.
     *
     * @return true if the hand has a flush, false if it does not.
     */
    public boolean checkFlush() {
        if (hand.stream().filter(p -> p.getSuit() == '♠').count() > 4) {
            return true;
        }
        if (hand.stream().filter(p -> p.getSuit() == '♥').count() > 4) {
            return true;
        }
        if (hand.stream().filter(p -> p.getSuit() == '♦').count() > 4) {
            return true;
        }
        return hand.stream().filter(p -> p.getSuit() == '♣').count() > 4;
    }

    /**
     * Returns the sum of all the faces in the hand.
     *
     * @return Integer sum of all faces in hand.
     */
    public int getSum() {
        return hand.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    /**
     * Returns all the PlayingCard's in hand of Hearts suit.
     *
     * @return String of all PlayingCards of hearts suit.
     */
    public StringBuilder getHearths() {
        StringBuilder hearths = new StringBuilder();
        hand.stream().filter(s -> s.getSuit() == '♥').map(PlayingCard::getAsString).sorted().forEach(s -> hearths.append(s).append(" "));

        if (hearths.toString().equals("")) {
            hearths.append("No hearts");
        }
        return hearths;
    }

    /**
     * Checks if the hand contains the Queen of spades.
     *
     * @return true if the Queen of spades is the hand.
     */
    public boolean isQueenOfSpade() {
        return hand.stream().anyMatch(s -> s.getSuit() == '♠' && s.getFace() == 12);
    }
}
